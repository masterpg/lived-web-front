import { LitElement } from 'lit-element';
export declare class CommAwesomeElement extends LitElement {
    render(): import("lit-element").TemplateResult;
    hot: boolean;
}
